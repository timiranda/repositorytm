package com.example.movieapp.moviesearch;

import com.example.movieapp.CallbackArrayResponse;
import com.example.movieapp.network.MovieDBService;

import java.util.List;

public class SearchMoviePresenter implements SearchMovieContract.Presenter{
    private SearchMovieContract.View view;
    private static MovieDBService instance;


    @Override
    public void setView(SearchMovieContract.View view) {
        this.view = view;
    }

    @Override
    public void OnButtonSearchClicked(final int movieId) {
        view.goToDetails(movieId);
    }


}
