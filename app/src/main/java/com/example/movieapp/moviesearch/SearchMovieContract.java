package com.example.movieapp.moviesearch;

import android.content.Context;

import com.example.movieapp.models.MovieItem;
import com.example.movieapp.roomdb.IMovieRepository;

import java.util.List;

public interface SearchMovieContract {

    interface View {
        void showError(String message);
        void goToDetails(int movieId);
    }

    interface Presenter {
        void setView(View view);
        void OnButtonSearchClicked(int movieId);
    }
}
