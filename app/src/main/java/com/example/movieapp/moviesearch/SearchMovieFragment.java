package com.example.movieapp.moviesearch;

import android.content.Context;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.movieapp.NavigateListener;
import com.example.movieapp.R;
import com.example.movieapp.databinding.FragmentSearchMovieBinding;


public class SearchMovieFragment extends Fragment implements SearchMovieContract.View, NavigateListener {

    private SearchMovieContract.Presenter mPresenter;
    private NavigateListener mActivityListener;
    private LinearLayout liProgress;
    private TextView tvError;
    private Button btSearch;



    public SearchMovieFragment() {
        // Required empty public constructor
    }


    public static SearchMovieFragment newInstance() {
        SearchMovieFragment fragment = new SearchMovieFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        mPresenter = new SearchMoviePresenter();
        mPresenter.setView(this);
        //LocaleHelper.onCreate(this.getActivity().getApplicationContext());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //View view = inflater.inflate(R.layout.fragment_movie_list, container, false);
        FragmentSearchMovieBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_movie, container, false);
        btSearch = binding.btSearch;
        final EditText etSearch = binding.etSearch;
        tvError = binding.tvError;


        btSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String numberMovie = etSearch.getText().toString();
                final int movieId = Integer.parseInt(numberMovie);
                mPresenter.OnButtonSearchClicked(movieId);
            }
        });
        return binding.getRoot();
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivityListener = (NavigateListener) context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivityListener = null;

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void showError(String message) {
        tvError.setText(message);
    }

    @Override
    public void goToDetails(int movieId) {
        mActivityListener.goToDetails(movieId);
    }

    @Override
    public void goToFavorites() {
        //do nothing
    }


    //é necessário declarar o onItemClick aqui e fazer implementaçao do onCickListener aqui no fragment para funcionar



    /* public interface NavigateListener {
        void goToDetails(int movieId);
    } */
}
