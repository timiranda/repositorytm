package com.example.movieapp;

public interface ItemClickListener {
    //get movie id on item click recyclerview
    void onItemClick(int movieId);
}
