package com.example.movieapp.movielists;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.movieapp.ItemClickListener;
import com.example.movieapp.NavigateListener;
import com.example.movieapp.PaginationScrollListener;
import com.example.movieapp.R;
import com.example.movieapp.ScrollToRightListener;
import com.example.movieapp.databinding.FragmentMovieListBinding;
import com.example.movieapp.models.MovieItem;

import java.util.List;

public class MovieListFragment extends Fragment implements MovieListContract.View, ItemClickListener, NavigateListener, ScrollToRightListener, SwipeRefreshLayout.OnRefreshListener {

    private MovieListContract.Presenter mPresenter;
    private NavigateListener mActivityListener;
    public MovieAdapter movieAdapter;
    private RecyclerView rvMovies;
    private LinearLayout liProgress, liError;
    private ImageView ivError;
    private TextView tvNowPlaying;
    private PaginationScrollListener paginationScrollListener;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int currentPage = 1;
    private Parcelable listState;
    private String language;




    public MovieListFragment() {
        // Required empty public constructor
    }


    public static MovieListFragment newInstance() {
        MovieListFragment fragment = new MovieListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mPresenter = new MovieListPresenter();
        mPresenter.setView(this);
        //LocaleHelper.onCreate(this.getActivity().getApplicationContext());

    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("ListState", rvMovies.getLayoutManager().onSaveInstanceState());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentMovieListBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie_list, container, false);
        rvMovies = binding.rvMovies;
        liProgress = binding.liProgress;
        tvNowPlaying = binding.tvNowplaying;
        swipeRefreshLayout = binding.swipeRefresh;
        liError = binding.liError;
        ivError = binding.ivError;

        setupContentView();

        if (savedInstanceState != null) {
            //to recover position from recyclerview
            listState = savedInstanceState.getParcelable("ListState");
            rvMovies.getLayoutManager().onRestoreInstanceState(listState);
        } else {
            //setupContentView();
            mPresenter.loadNowPlayingList(currentPage, language);
        }

        return binding.getRoot();
    }




    private void setupContentView() {
        //swiperefreshlayout for when user pulls up to refresh, and scroll to right listener to get next page of movies
        swipeRefreshLayout.setOnRefreshListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        paginationScrollListener = new PaginationScrollListener(linearLayoutManager, this) {
            @Override
            public void onScrollToRight() {
                mPresenter.onScrollToRight(language);
            }
        };
        rvMovies.setLayoutManager(linearLayoutManager);
        rvMovies.addOnScrollListener(paginationScrollListener);
        Glide.with(getContext())
                .load(R.drawable.baseline_movie_creation_white_bigger)
                .placeholder(R.drawable.baseline_movie_creation_white_bigger)
                .into(ivError);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivityListener = (NavigateListener) context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivityListener = null;

    }


    @Override
    public void onStart() {
        super.onStart();
        //mPresenter.loadNowPlayingList(currentPage);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            tvNowPlaying.setVisibility(View.GONE);

        } else {
            tvNowPlaying.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showError(String message) {
        liProgress.setVisibility(View.GONE);
        liError.setVisibility(View.VISIBLE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.errormovielist)
                .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void goToDetails(int movieId) {
        mActivityListener.goToDetails(movieId);
    }

    @Override
    public void goToFavorites() {
        //do nothing
    }


    @Override
    public void showLoading() {
        liProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNowPlayingList(final List<MovieItem> movieList) {
        liProgress.setVisibility(View.GONE);

        //when movie adapter is null, create one
        if (movieAdapter == null) {
            movieAdapter = new MovieAdapter(movieList, this.getActivity(), this);
            rvMovies.setAdapter(movieAdapter);
            // TODO: 16/12/2019 search item decorator 

            //when not null, keep same adapter and add more movie items to list
        } else {
            movieAdapter.addItems(movieList);
            movieAdapter.notifyDataSetChanged();
        }

        //delay for swiperefresh
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 1500);

    }

    @Override
    public void onItemClick(int movieId) {
        mActivityListener.goToDetails(movieId);
    }


    @Override
    public void onRefresh() {
        paginationScrollListener.onRefresh();
        mPresenter.onPullToRefresh();
    }

    @Override
    public void onScrollToRight() {
        mPresenter.onScrollToRight(language);
    }
}
