package com.example.movieapp.movielists;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.example.movieapp.BaseViewHolder;
import com.example.movieapp.ItemClickListener;
import com.example.movieapp.R;
import com.example.movieapp.databinding.MovieItemBinding;
import com.example.movieapp.databinding.MovieItemEvenBinding;
import com.example.movieapp.models.MovieItem;

import org.w3c.dom.Text;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<MovieItem> movieLists;
    private Context context;
    private ItemClickListener itemClickListener;
    private static final int VIEW_TYPE_EVEN = 0;
    private static final int VIEW_TYPE_NORMAL = 1;

    public MovieAdapter(List<MovieItem> movieLists, Context context, ItemClickListener itemClickListener) {
        this.movieLists = movieLists;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    //view holder base

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        //applying different viewholders for different view types
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                MovieItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.movie_item, parent, false);
                return new MovieViewHolder(binding);
            case VIEW_TYPE_EVEN:
                MovieItemEvenBinding bindingEven = DataBindingUtil.inflate(layoutInflater, R.layout.movie_item_even, parent, false);
                return new ProgressHolder(bindingEven);
                default:
                    return null;
        }
    }


    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return VIEW_TYPE_EVEN;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

    @Override
    public int getItemCount() {
        return movieLists.size();
    }


    public void addItems(List<MovieItem> movieItemList) {
        movieLists.addAll(movieItemList);
        notifyDataSetChanged();
    }


    public void clear() {
        movieLists.clear();
        notifyDataSetChanged();
    }

    /* MovieItem getItem(int position) {
        return movieLists.get(position);
    } */


    //classe vie holder base, pode estar em ficheiro a parte ou nao


    //movie view holder

    public class MovieViewHolder extends BaseViewHolder implements View.OnClickListener{

        private final MovieItemBinding binding;
        private ImageView ivItemPoster;
        private TextView tvItemName;
        private RatingBar rbVote;

        public MovieViewHolder(MovieItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.getRoot().setOnClickListener(this);
            ivItemPoster = binding.ivItemposter;
            tvItemName = binding.tvItemname;
            rbVote = binding.rbVote;
        }

        @Override
        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);
            MovieItem movieItem = movieLists.get(position);
            String imageUrl = "https://image.tmdb.org/t/p/w342" + movieItem.getPosterPath();
            /* RequestManager requestManager = Glide.with(context);
            RequestBuilder requestBuilder = requestManager.load(imageUrl);
            requestBuilder.into(ivItemPoster); */
            Glide.with(context)
                    .load(imageUrl)
                    .placeholder(R.drawable.baseline_movie_creation_white_bigger)
                    .into(ivItemPoster);
            binding.setMovie(movieItem);
        }

        @Override
        public void onClick(View view) {
            final int movieId = movieLists.get(getAdapterPosition()).getId();
            itemClickListener.onItemClick(movieId);
        }
    }




    //progress view holder

    public class ProgressHolder extends BaseViewHolder implements View.OnClickListener {

        private MovieItemEvenBinding binding1;
        TextView tvName;
        ImageView ivPoster;
        RatingBar rbVote;

        public ProgressHolder(MovieItemEvenBinding binding1) {
            super(binding1.getRoot());
            this.binding1 = binding1;
            this.binding1.getRoot().setOnClickListener(this);
            tvName = binding1.tvItemname;
            rbVote = binding1.rbVote;
            ivPoster = binding1.ivItemposter;
        }

        @Override
        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);
            MovieItem movieItem = movieLists.get(position);
            binding1.setMovie(movieItem);
            String imageUrl = "https://image.tmdb.org/t/p/w342" + movieItem.getPosterPath();
            /* RequestManager requestManager = Glide.with(context);
            RequestBuilder requestBuilder = requestManager.load(imageUrl);
            requestBuilder.into(ivPoster); */
            Glide.with(context)
                    .load(imageUrl)
                    .placeholder(R.drawable.baseline_movie_creation_white_bigger)
                    .into(ivPoster);
        }

        @Override
        public void onClick(View view) {
            final int movieId = movieLists.get(getAdapterPosition()).getId();
            itemClickListener.onItemClick(movieId);
        }
    }

}
