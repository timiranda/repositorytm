package com.example.movieapp.movielists;

import android.content.Context;

import com.example.movieapp.models.MovieItem;
import com.example.movieapp.roomdb.IMovieRepository;

import java.util.List;

public interface MovieListContract {

    interface View {
        void showError(String message);
        void goToDetails(int movieId);
        void showLoading();
        void showNowPlayingList(List<MovieItem> movieList);
    }

    interface Presenter {
        void setView(View view);
        void loadNowPlayingList(Integer page, String language);
        void onPullToRefresh();
        void onScrollToRight(String language);
        //void loadMoreMovies();
    }
}
