package com.example.movieapp.movielists;

import com.example.movieapp.CallbackArrayResponse;
import com.example.movieapp.ItemClickListener;
import com.example.movieapp.network.MovieDBService;

import java.util.List;

public class MovieListPresenter implements MovieListContract.Presenter, ItemClickListener {
    private MovieListContract.View view;
    private static MovieDBService instance;
    private int currentPage = 1;
    private String language;


    @Override
    public void setView(MovieListContract.View view) {
        this.view = view;
    }


    @Override
    public void loadNowPlayingList(final Integer page, String language) {

        view.showLoading();

        instance = MovieDBService.getInstance();

            instance.loadNowPlayingList(new CallbackArrayResponse() {
                @Override
                public void onSuccess(List response) {

                    view.showNowPlayingList(response);
                }

                @Override
                public void onError(String errorMessage) {
                    view.showError(errorMessage);
                }
            }, page, language);

            currentPage = page;
        }

    @Override
    public void onPullToRefresh() {
        currentPage = 1;
        loadNowPlayingList(currentPage, language);
    }

    @Override
    public void onScrollToRight(String language) {
        currentPage++;
        loadNowPlayingList(currentPage, language);
    }

    @Override
    public void onItemClick(int movieId) {
        view.goToDetails(movieId);
    }


}
