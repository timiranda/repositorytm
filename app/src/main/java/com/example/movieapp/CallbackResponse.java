package com.example.movieapp;

public interface CallbackResponse {
    void onSuccess(Object response);
    void onError(String errorMessage);
}
