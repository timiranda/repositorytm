package com.example.movieapp;

public interface NavigateListener {
    void goToDetails(int movieId);

    void goToFavorites();

}
