package com.example.movieapp.moviedetails;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.movieapp.R;
import com.example.movieapp.databinding.FragmentDetailBinding;
import com.example.movieapp.models.Genre;
import com.example.movieapp.models.Movie;
import com.example.movieapp.roomdb.MovieRepository;



public class DetailFragment extends Fragment implements DetailContract.View {

    private static final String MOVIE_ID = "movie_id";
    private int movieId;
    private TextView tvMovieName, tvOverview, tvGenre, tvDuration, tvLanguage;
    private ImageView ivPoster;
    private LinearLayout liProgress, liError;
    private ImageButton btFavorites, btRemove;
    private ImageView ivError;
    private DetailContract.Presenter mPresenter;
    private String language;


    public DetailFragment() {
        // Required empty public constructor
    }


    public static DetailFragment newInstance(int movieId) {
        //bundle to pass int from another fragment
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putInt(MOVIE_ID, movieId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //keeps fragment state saved
        setRetainInstance(true);

        if (getArguments() != null) {
            movieId = getArguments().getInt(MOVIE_ID);
        }

        mPresenter = new DetailPresenter();
        mPresenter.setView(this);
        mPresenter.setRepository(MovieRepository.getInstance(this.getActivity().getApplicationContext()));
        //LocaleHelper.onCreate(this.getActivity().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentDetailBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false);
        tvMovieName = binding.tvMoviename;
        tvOverview = binding.tvOverview;
        tvGenre = binding.tvGenre;
        tvDuration = binding.tvDuration;
        tvLanguage = binding.tvLanguage;
        ivPoster = binding.ivPoster;
        liProgress = binding.liProgress;
        btFavorites = binding.btFavorite;
        btRemove = binding.btRemove;
        liError = binding.liError;
        ivError = binding.ivError;

        Glide.with(getContext())
                .load(R.drawable.baseline_movie_creation_white_bigger)
                .placeholder(R.drawable.baseline_movie_creation_white_bigger)
                .into(ivError);

        return binding.getRoot();
    }


    @Override
    public void onStart() {
        super.onStart();
        mPresenter.showDetails(movieId, language);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void showMovieDetails(final Movie movie) {
        liProgress.setVisibility(View.GONE);
        tvMovieName.setText(movie.getTitle());
        tvDuration.setText(movie.getRuntime() == null ? "-" : movie.getRuntime().toString() + "min");
        tvGenre.setText(getGenres(movie));
        tvLanguage.setText(movie.getOriginalLanguage());
        tvOverview.setText(movie.getOverview());
        String imageUrl = "https://image.tmdb.org/t/p/w342" + movie.getPosterPath();
        Glide.with(this)
                .load(imageUrl)
                .into(ivPoster);


        //favorite buttons
        btFavorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.onFavoriteButtonClicked(movie);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.addedfavorites)
                        .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                dialog.setCanceledOnTouchOutside(true);
                // TODO: 16/12/2019 adicionar retorno de inserção e retirar as duas linhas de baixo para novo método mediante resultado
                //btFavorites.setVisibility(View.GONE);
                //btRemove.setVisibility(View.VISIBLE);
                //Toast.makeText(getActivity(), "Added to Favorites", Toast.LENGTH_SHORT).show();
            }
        });

        btRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.suredeletefavorites)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mPresenter.onRemoveButtonClicked(movie);
                                // TODO: 16/12/2019 adicionar retorno de remover e retirar as duas linhas de baixo para novo método mediante resultado
                                //btRemove.setVisibility(View.GONE);
                                //btFavorites.setVisibility(View.VISIBLE);
                                //Toast.makeText(getActivity(), "Removed from Favorites", Toast.LENGTH_SHORT).show();

                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        dialog.setCanceledOnTouchOutside(true);

            }
        });

    }

    @Override
    public void showError(String message) {
        liProgress.setVisibility(View.GONE);
        liError.setVisibility(View.VISIBLE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.errormoviedetails)
                .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
    }

    @Override
    public void showLoading() {
        liProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFavoriteButton() {
        btRemove.setVisibility(View.VISIBLE);
        btFavorites.setVisibility(View.GONE);
    }

    @Override
    public void hideFavoriteButton() {
        btRemove.setVisibility(View.GONE);
        btFavorites.setVisibility(View.VISIBLE);
    }

    @Override
    public void showDialogueRemoved() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.removed)
                .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
    }

    @Override
    public void showNetworkError() {
        liProgress.setVisibility(View.GONE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.errorinternetconnection)
                .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);

    }


    //genre string

    private String getGenres(Movie movie) {
        String genres = "";

        for (int i = 0; i < movie.getGenres().size(); i++) {
            Genre genre = movie.getGenres().get(i);
            genres += genre.getName() + ", ";
        }

        genres = removeTrailingComma(genres);


        return genres.isEmpty()?"-":genres;
    }


    @NonNull
    private String removeTrailingComma(String text) {
        text = text.trim();
        if (text.endsWith(",")) {
            text = text.substring(0, text.length() - 1);
        }
        return text;
    }


}
