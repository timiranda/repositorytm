package com.example.movieapp.moviedetails;

import com.example.movieapp.CallbackResponse;
import com.example.movieapp.models.Movie;
import com.example.movieapp.network.MovieDBService;
import com.example.movieapp.roomdb.IMovieRepository;


public class DetailPresenter implements DetailContract.Presenter{
    private DetailContract.View view;
    private static MovieDBService instance;
    private IMovieRepository repository;


    @Override
    public void setView(DetailContract.View view) {
        this.view = view;
        instance = MovieDBService.getInstance();
    }

    @Override
    public void setRepository(IMovieRepository repository) {
        this.repository = repository;
    }


    @Override
    public void showDetails(final Integer movieId, final String language) {
        view.showLoading();
        repository.getFavoriteMovieById(movieId, new CallbackResponse() {
            @Override
            public void onSuccess(Object response) {
                view.showFavoriteButton();
                view.showMovieDetails((Movie) response);
                //update movie with api request
                instance.loadMovieDetails(movieId, language, new CallbackResponse() {
                    @Override
                    public void onSuccess(Object response) {
                        //possible error doing request
                        repository.updateMovie((Movie) response);
                        view.showMovieDetails((Movie) response);
                    }

                    @Override
                    public void onError(String errorMessage) {
                        //view.showError(errorMessage);
                        view.showNetworkError();

                    }
                });


            }

            @Override
            public void onError(String errorMessage) {
                //if the movie doesn't exist in db, an api request will be made
                instance.loadMovieDetails(movieId, language, new CallbackResponse() {
                    @Override
                    public void onSuccess(Object response) {
                        view.hideFavoriteButton();
                        view.showMovieDetails((Movie) response);
                    }

                    @Override
                    public void onError(String errorMessage) {
                        view.showError(errorMessage);

                    }
                });
            }
        });


    }


    @Override
    public void onFavoriteButtonClicked(Movie movie) {
        repository.insertMovie(movie);
        view.showFavoriteButton();
    }

    @Override
    public void onRemoveButtonClicked(Movie movie) {
        repository.removeMovie(movie);
        view.hideFavoriteButton();
        view.showDialogueRemoved();
    }


}
