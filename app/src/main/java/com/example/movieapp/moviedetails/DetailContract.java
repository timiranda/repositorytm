package com.example.movieapp.moviedetails;

import com.example.movieapp.models.Genre;
import com.example.movieapp.models.Movie;
import com.example.movieapp.roomdb.IMovieRepository;
import com.example.movieapp.roomdb.MovieRepository;

import java.io.IOException;

public interface DetailContract {
    interface View {
        void showMovieDetails(Movie movie);
        void showError(String message);
        void showLoading();
        void showFavoriteButton();
        void hideFavoriteButton();
        void showDialogueRemoved();
        void showNetworkError();
    }

    interface Presenter {
        void setView(View view);
        void showDetails(Integer movieId, String language);
        void setRepository(IMovieRepository repository);
        void onFavoriteButtonClicked(Movie movie);
        void onRemoveButtonClicked(Movie movie);
    }
}
