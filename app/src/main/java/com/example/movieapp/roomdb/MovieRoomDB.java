package com.example.movieapp.roomdb;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.movieapp.ConvertListJson;
import com.example.movieapp.models.Movie;

@Database(entities = {Movie.class}, version = 1)
@TypeConverters({ConvertListJson.class})
public abstract class MovieRoomDB extends RoomDatabase {

     public abstract MovieDao movieDao();

    private static volatile MovieRoomDB instance;

    //migration detail - next to add

    //db singleton

    public static MovieRoomDB getDatabase(final Context context) {
        if (instance == null) {
            synchronized (MovieRoomDB.class) {
                    instance = Room.databaseBuilder(context.getApplicationContext(), MovieRoomDB.class, "movie_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
            }
        }
        return instance;
    }

    //execute db asynctask

    public static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback(){

        @Override
        public void onOpen (@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            new PopulateDbAsync(instance).execute();

        }
    };

    public static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final MovieDao dao;

        PopulateDbAsync(MovieRoomDB db) {
            dao = db.movieDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dao.insertMovie();
            return null;
        }
    }

}

