package com.example.movieapp.roomdb;

import com.example.movieapp.CallbackArrayResponse;
import com.example.movieapp.CallbackResponse;
import com.example.movieapp.models.Movie;
import com.example.movieapp.models.MovieItem;

import java.util.List;

public interface IMovieRepository {

    void getFavoritesList(CallbackArrayResponse response);
    void getFavoriteMovieById(Integer movieId, CallbackResponse response);
    void insertMovie(Movie movie);
    void removeMovie(Movie movie);
    void updateMovie(Movie movie);
}
