package com.example.movieapp.roomdb;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;


import com.example.movieapp.CallbackArrayResponse;
import com.example.movieapp.CallbackResponse;
import com.example.movieapp.MainActivity;
import com.example.movieapp.R;
import com.example.movieapp.models.Movie;

import java.util.List;

public class MovieRepository implements IMovieRepository {
    private MovieDao movieDao;
    private List<Movie> mAllMovies;
    MovieRoomDB db;
    private Context context;
    private final static String TAG = "MovieRepository";
    private static IMovieRepository instance;
    private CallbackArrayResponse callbackArrayResponse;
    private CallbackResponse callbackResponse;


    MovieRepository(Context context) {
        db = MovieRoomDB.getDatabase(context);
        movieDao = db.movieDao();
        context.getApplicationContext();
    }

    public static IMovieRepository getInstance(Context context) {
        if (instance == null) {
            instance = new MovieRepository(context);
        }

        return  instance;

    }


    //movies

    //insert movie

    @Override
    public void insertMovie(Movie movie) {
        new InsertAsyncTaskMovie(movieDao).execute(movie);
    }

    //remove movie

    @Override
    public void removeMovie(Movie movie) {
        new RemoveAsyncTaskMovie(movieDao).execute(movie);
    }


    //update movie


    @Override
    public void updateMovie(Movie movie) {
        new UpdateAsyncTaskMovie(movieDao).execute(movie);
    }


    //get list movies

    @Override
    public void getFavoritesList(CallbackArrayResponse response) {
        //getmAllMovies();
        callbackArrayResponse = response;
        new GetAsyncTaskMovie(movieDao).execute();
    }

    //get by id

    @Override
    public void getFavoriteMovieById(Integer movieId, CallbackResponse response) {
        callbackResponse = response;
        new GetAsyncTaskMovieById(movieDao).execute(movieId);
    }





    //insert movie


    private static class InsertAsyncTaskMovie extends AsyncTask<Movie, Void, Void> {

        private MovieDao mAsyncTaskDao;

        InsertAsyncTaskMovie(MovieDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Movie... movies) {
            mAsyncTaskDao.insertMovie(movies[0]);
            Log.d("VerFilme", "Name: " + movies[0].getTitle() + " Genero 1:  " + movies[0].getGenres().get(0).getName());

            return null;
        }
    }

    //get movies

    private  class GetAsyncTaskMovie extends AsyncTask<Void, Void, List<Movie>> {

        private MovieDao mAsyncTaskDao;

        GetAsyncTaskMovie (MovieDao dao) {
            mAsyncTaskDao = dao;
        }


        @Override
        protected List<Movie> doInBackground(Void... voids) {
            return mAsyncTaskDao.getAll();
        }

        @Override
        protected void onPostExecute(List<Movie> movieList) {
            super.onPostExecute(movieList);
            if (movieList == null || movieList.isEmpty()) {
                callbackArrayResponse.onError("Your Favorite list is empty! Check our Now playing list to add some movies.");
            } else if (movieList != null || movieList.size() > 0){
                callbackArrayResponse.onSuccess(movieList);
            } else {
                callbackArrayResponse.onError("Error getting favorites");
            }
        }
    }


    //remove movie

    private static class RemoveAsyncTaskMovie extends AsyncTask<Movie, Void, Void> {

        private MovieDao mAsyncTaskDao;

        RemoveAsyncTaskMovie(MovieDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Movie... movies) {
            mAsyncTaskDao.delete(movies[0]);
//            Log.d("VerFilme", "Name: " + movies[0].getTitle() + " Genero 1:  " + movies[0].getGenres().get(0).getName());

            return null;
        }
    }


    //update movie

    private static class UpdateAsyncTaskMovie extends AsyncTask<Movie, Void, Void> {

        private MovieDao mAsyncTaskDao;

        UpdateAsyncTaskMovie(MovieDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Movie... movies) {
            mAsyncTaskDao.update(movies[0]);
//            Log.d("VerFilme", "Name: " + movies[0].getTitle() + " Genero 1:  " + movies[0].getGenres().get(0).getName());

            return null;
        }
    }


    //get movie by id

    private class GetAsyncTaskMovieById extends AsyncTask<Integer, Void, Movie> {

        private MovieDao mAsyncTaskDao;

        GetAsyncTaskMovieById (MovieDao dao) {
            mAsyncTaskDao = dao;
        }


        @Override
        protected Movie doInBackground(Integer... integers) {
            return mAsyncTaskDao.loadMovieById(integers[0]);
        }

        @Override
        protected void onPostExecute(Movie movie) {
            super.onPostExecute(movie);
            if (movie == null) {
                callbackResponse.onError("Error getting movie");
            } else {
                callbackResponse.onSuccess(movie);
            }
        }
    }

}








//movieitems
//a minha questao e em vez de tratar o movieitem como um objecto, ele ser tratado como uma lista de objectos atraves do moviesresponse

    /* List<MovieItem> getmAllMovieItems() {
        return mAllMovieItems;
    }

    public void insertItem(MovieItem movieItem) {
        new InsertAsyncTaskMovieItem(movieDao).execute(movieItem);

    }

    public void insertList(List<MovieItem> movieItemList) {
        new InsertAsyncTaskMovieItem(movieDao).execute(movieItemList);
    }

    private static class InsertAsyncTaskMovieItem extends AsyncTask<List<MovieItem>, Void, Void>{

        private MovieDao mAsyncTaskDao;

        InsertAsyncTaskMovieItem(MovieDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(List<MovieItem>... movieItemList) {
            mAsyncTaskDao.insertItemList(movieItemList);
            return null;
        }
    } */

    /* @Override
    public void getNowPlayingList(final List<MovieItem> movieItemList) {

        instance = MovieDBService.getInstance();

        if (isConnected) {
            instance.loadNowPlayingList(new CallbackArrayResponse() {
                @Override
                public void onSuccess(List response) {
                    movieItemList.equals(response);
                    movieDao.insertItemList(movieItemList);
                    //if internet connection is true, vai buscar a response ao moviedbservice
                    //ao buscar, insere na base de daods
                    //se nao, vai buscar a informaçao a base de dados
                    //se base de dados estiver vazia, nao apresentar nada
                }

                @Override
                public void onError(String errorMessage) {
                    movieItemList.isEmpty();
                }
            });

        } else {
            getmAllMovieItems();
        }
    }



    @Override
    public void getDetails(int movieId) {

    } */

//checar internet

/*     ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo info = connectivityManager.getActiveNetworkInfo();
    boolean isConnected = info != null && info.isConnectedOrConnecting(); */
