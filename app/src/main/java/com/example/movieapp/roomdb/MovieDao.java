package com.example.movieapp.roomdb;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.example.movieapp.models.Movie;
import com.example.movieapp.models.MovieItem;
import com.example.movieapp.models.MoviesResponse;

import java.util.List;

@Dao
public interface MovieDao {
    //for movielist
    //@Transaction
    @Query("SELECT * FROM movie")
    List<Movie> getAll();
    //Movie[] getAll();

    //@Transaction
    @Query("SELECT * FROM movie WHERE mId IN (:movieId)")
    Movie loadMovieById(int movieId);

    //@Transaction
    @Query("SELECT * FROM movie WHERE mId IN (:movieIds)")
    List<Movie> loadAllByIds(Integer[] movieIds);

    //@Transaction
    @Query("SELECT * FROM movie WHERE title LIKE :title")
    Movie findByTitle(String title);

    @Query("DELETE FROM movie")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMovie(Movie...movies);

    @Update
    void update(Movie...movies);

    @Delete
    void delete(Movie movie);

}
