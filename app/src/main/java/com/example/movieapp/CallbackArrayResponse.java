package com.example.movieapp;

import java.util.List;

public interface CallbackArrayResponse {
    void onSuccess(List response);
    void onError(String errorMessage);
}