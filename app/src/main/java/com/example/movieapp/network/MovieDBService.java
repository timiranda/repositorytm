package com.example.movieapp.network;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import com.example.movieapp.CallbackArrayResponse;
import com.example.movieapp.CallbackResponse;
import com.example.movieapp.R;
import com.example.movieapp.models.Movie;
import com.example.movieapp.models.MovieItem;
import com.example.movieapp.models.MoviesResponse;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieDBService {

    private static final String TAG = "MovieDBService";

    private static MovieDBService instance;
    private static IMovieDBService service;
    //private String errorMessage = "An error occurred";
    private String errorMessage;
    private CallbackResponse callbackResponse;
    private CallbackArrayResponse callbackArrayResponse;
    private static String apiKey = "b6a74b93fa69a6daaa21497e06975055";
    private String language = Resources.getSystem().getConfiguration().locale.getLanguage();


    //movieservice singleton

    public MovieDBService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(IMovieDBService.class);
    }

    public static MovieDBService getInstance() {
        if (instance == null) {
            instance = new MovieDBService();
        }

        return instance;
    }

    //movie

    public void loadMovieDetails(final Integer movieId, String language, CallbackResponse callbackResponse) {
        this.callbackResponse = callbackResponse;
        if (language == null) {
            language = Resources.getSystem().getConfiguration().locale.getLanguage();
        }
        this.language = language;
        new MovieFromAPI().execute(movieId);

    }

    //list

    public void loadNowPlayingList(CallbackArrayResponse callbackResponse, Integer page, String language) {
        this.callbackArrayResponse = callbackResponse;
        if (language == null) {
            language = Resources.getSystem().getConfiguration().locale.getLanguage();
        }
        this.language = language;
        new NowPlayingListFromAPI().execute(page);
    }


    //Movie

    private class MovieFromAPI extends AsyncTask<Integer, Void, Movie> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Movie doInBackground(Integer... movieId) {
            try {
                return instance.getMovieById2(movieId[0], language);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Movie movie) {
            super.onPostExecute(movie);
            if(movie != null){
                callbackResponse.onSuccess(movie);
                //view.showMovieDetails(movie);
            } else {
                callbackResponse.onError(errorMessage);
                //view.showError("Error in loading movie");
            }
        }
    }

    //List


    private class NowPlayingListFromAPI extends AsyncTask<Integer, Void, List<MovieItem>> {

       


        @Override
        protected List<MovieItem> doInBackground(Integer... page) {
            try {
                return instance.getNowPlayingList(page[0], language);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(List<MovieItem> movieItemList) {
            super.onPostExecute(movieItemList);
            if (movieItemList != null) {
                callbackArrayResponse.onSuccess(movieItemList);
            } else {
                callbackArrayResponse.onError(errorMessage);
            }
        }
    }



//versao sincrona
    //movie

    public Movie getMovieById2(Integer movieId, String language) throws IOException {
        this.language = language;
        Call<Movie> call = service.getMovie(movieId, apiKey, language);
        Response<Movie> response = call.execute();
        Movie movie = null;
        if(response.code() == 200){
            movie = response.body();
            errorMessage = "";
        } else {
            Log.e(TAG, "Code = " + response.code() + " message = "+response.message());
            //errorMessage = response.code() + response.message();
            errorMessage = response.code() + response.message();
        }
        return  movie;

    }

    //list



    public List<MovieItem> getNowPlayingList(Integer page, String language) throws IOException {
        this.language = language;
        Call<MoviesResponse> call = service.getNowPlayingList(apiKey, page, language);
        Response<MoviesResponse> response = call.execute();
        MoviesResponse movieItemList = null;
        if (response.code() == 200) {

            movieItemList = response.body();
            errorMessage = "";
        } else {
            Log.e(TAG, "Code = " + response.code() + " message = "+response.message());
            //errorMessage = response.code() + response.message();
            errorMessage = response.code() + response.message();
        }

        return movieItemList.getMovieItems();
    }


}
