package com.example.movieapp.network;

import com.example.movieapp.models.Movie;
import com.example.movieapp.models.MovieItem;
import com.example.movieapp.models.MoviesResponse;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IMovieDBService {

    @GET("movie/{movie_id}")
    Call<Movie> getMovie(@Path("movie_id") Integer movieId, @Query("api_key") String apiKey, @Query("language") String language);

    @GET("movie/now_playing")
    Call<MoviesResponse> getNowPlayingList(@Query("api_key") String apiKey, @Query("page") Integer page, @Query("language") String language);

}


