package com.example.movieapp;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener implements ScrollToRightListener {

    private LinearLayoutManager layoutManager;
    private ScrollToRightListener scrollToRightListener;

    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 3;
    private int firstVisibleItem, visibleItemCount, totalItemCount;


    public PaginationScrollListener(LinearLayoutManager layoutManager, ScrollToRightListener scrollToRightListener) {
        this.layoutManager = layoutManager;
        this.scrollToRightListener = scrollToRightListener;
    }

    public void onRefresh() {
        previousTotal = 0;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = layoutManager.getItemCount();
        firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

        //o loading so serve para nao carregar logo as paginas todas de uma vez
        // vai carregando uma a uma a medida que se faz o scroll
        //quando carrega nova pagina, totalitemcount passa a 40 mas previous mantem-se a 20, o loading passa a ser false
        //nao carrega mais elementos ate chegar ao 15º / 16º

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        if (!loading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {
            this.scrollToRightListener.onScrollToRight();
            loading = true;
        }
    }
}
