package com.example.movieapp;

import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.example.movieapp.models.Genre;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ConvertListJson {
    //converte a lista em string para formato lista

     @TypeConverter
     public static List<Genre> fromString(String genresNames){
        Type listType = new TypeToken<List<Genre>>() {}.getType();
        return new Gson().fromJson(genresNames, listType);
    }
    //converte a lista para string

    @TypeConverter
    public static String fromList(List<Genre> mGenres) {
        Gson gson = new Gson();
        String json = gson.toJson(mGenres);
        return json;
    }

    //ver bem ultimo link favoritos
}
