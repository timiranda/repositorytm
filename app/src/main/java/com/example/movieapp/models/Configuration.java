
package com.example.movieapp.models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Configuration {

    //this object is used to configurate image url's - not needed, for now

    @SerializedName("change_keys")
    private List<String> mChangeKeys;
    @SerializedName("images")
    private Images mImages;

    public List<String> getChangeKeys() {
        return mChangeKeys;
    }

    public void setChangeKeys(List<String> changeKeys) {
        mChangeKeys = changeKeys;
    }

    public Images getImages() {
        return mImages;
    }

    public void setImages(Images images) {
        mImages = images;
    }

}
