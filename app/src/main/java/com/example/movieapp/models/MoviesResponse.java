package com.example.movieapp.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity
public class MoviesResponse {

    @PrimaryKey
    @ColumnInfo(name = "results")
    @SerializedName("results")
    List<MovieItem> movieItems;

    public List<MovieItem> getMovieItems() {
        return movieItems;
    }
}
