
package com.example.movieapp.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
//@Entity
public class MovieItem {

    //@ColumnInfo(name = "adult")
    @SerializedName("adult")
    private Boolean mAdult;

    //@ColumnInfo(name = "backdrop_path")
    @SerializedName("backdrop_path")
    private String mBackdropPath;

    //@ColumnInfo(name = "genre_ids")
    @SerializedName("genre_ids")
    private List<Long> mGenreIds;

    //@PrimaryKey
    @SerializedName("id")
    private Integer mId;

    //@ColumnInfo(name = "original_language")
    @SerializedName("original_language")
    private String mOriginalLanguage;

    //@ColumnInfo(name = "original_title")
    @SerializedName("original_title")
    private String mOriginalTitle;

    //@ColumnInfo(name = "overview")
    @SerializedName("overview")
    private String mOverview;

    //@ColumnInfo(name = "popularity")
    @SerializedName("popularity")
    private Double mPopularity;

    //@ColumnInfo(name = "poster_path")
    @SerializedName("poster_path")
    private String mPosterPath;

    //@ColumnInfo(name = "release_date")
    @SerializedName("release_date")
    private String mReleaseDate;

    //@ColumnInfo(name = "title")
    @SerializedName("title")
    private String mTitle;

    //@ColumnInfo(name = "video")
    @SerializedName("video")
    private Boolean mVideo;

    //@ColumnInfo(name = "vote_average")
    @SerializedName("vote_average")
    private Double mVoteAverage;

    //@ColumnInfo(name = "vote_count")
    @SerializedName("vote_count")
    private Long mVoteCount;

    public Boolean getAdult() {
        return mAdult;
    }

    public void setAdult(Boolean adult) {
        mAdult = adult;
    }

    public String getBackdropPath() {
        return mBackdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        mBackdropPath = backdropPath;
    }

    public List<Long> getGenreIds() {
        return mGenreIds;
    }

    public void setGenreIds(List<Long> genreIds) {
        mGenreIds = genreIds;
    }

    public int getId() {
        return mId;
    }

    public void setId(Integer id) {
        mId = id;
    }

    public String getOriginalLanguage() {
        return mOriginalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        mOriginalLanguage = originalLanguage;
    }

    public String getOriginalTitle() {
        return mOriginalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        mOriginalTitle = originalTitle;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverview(String overview) {
        mOverview = overview;
    }

    public Double getPopularity() {
        return mPopularity;
    }

    public void setPopularity(Double popularity) {
        mPopularity = popularity;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public void setPosterPath(String posterPath) {
        mPosterPath = posterPath;
    }

    public String getReleaseDate() {
        return mReleaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        mReleaseDate = releaseDate;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Boolean getVideo() {
        return mVideo;
    }

    public void setVideo(Boolean video) {
        mVideo = video;
    }

    public Double getVoteAverage() {
        return mVoteAverage;
    }

    public void setVoteAverage(Double voteAverage) {
        mVoteAverage = voteAverage;
    }

    public Long getVoteCount() {
        return mVoteCount;
    }

    public void setVoteCount(Long voteCount) {
        mVoteCount = voteCount;
    }

}
