
package com.example.movieapp.models;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
@Entity(tableName = "genre")
public class Genre {

    @PrimaryKey
    @SerializedName("id")
    private Long mId;

    @ColumnInfo(name = "name")
    @SerializedName("name")
    private String mName;

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "mId=" + mId +
                ", mName='" + mName + '\'' +
                '}';
    }
}
