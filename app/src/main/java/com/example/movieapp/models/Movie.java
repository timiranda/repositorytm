
package com.example.movieapp.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.ArrayList;
import java.util.List;

import com.example.movieapp.ConvertListJson;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
@Entity(tableName = "movie")
public class Movie {

    @ColumnInfo(name = "adult")
    @SerializedName("adult")
    private Boolean mAdult;

    @ColumnInfo(name = "backdrop_path")
    @SerializedName("backdrop_path")
    private String mBackdropPath;

    @Ignore
    @SerializedName("belongs_to_collection")
    private BelongsToCollection mBelongsToCollection;

    @ColumnInfo(name = "budget")
    @SerializedName("budget")
    private Long mBudget;

    @TypeConverters(ConvertListJson.class)
    //@Ignore
    @ColumnInfo(name = "genres")
    @SerializedName("genres")
    private List<Genre> mGenres = new ArrayList<>();


    //converter string, nao interessa
    /*
    @TypeConverters(ConvertListJson.class)
    @ColumnInfo(name = "genres")
    private String genresNames; */

    @ColumnInfo(name = "homepage")
    @SerializedName("homepage")
    private String mHomepage;

    @PrimaryKey
    @SerializedName("id")
    private int mId;

    @ColumnInfo(name = "imdb_id")
    @SerializedName("imdb_id")
    private String mImdbId;

    @ColumnInfo(name = "original_language")
    @SerializedName("original_language")
    private String mOriginalLanguage;

    @ColumnInfo(name = "original_title")
    @SerializedName("original_title")
    private String mOriginalTitle;

    @ColumnInfo(name = "overview")
    @SerializedName("overview")
    private String mOverview;

    @ColumnInfo(name = "popularity")
    @SerializedName("popularity")
    private Double mPopularity;

    @ColumnInfo(name = "poster_path")
    @SerializedName("poster_path")
    private String mPosterPath;

    @Ignore
    @SerializedName("production_companies")
    private List<ProductionCompany> mProductionCompanies;

    @Ignore
    @SerializedName("production_countries")
    private List<ProductionCountry> mProductionCountries;

    @ColumnInfo(name = "release_date")
    @SerializedName("release_date")
    private String mReleaseDate;

    @ColumnInfo(name = "revenue")
    @SerializedName("revenue")
    private Long mRevenue;

    @ColumnInfo(name = "runtime")
    @SerializedName("runtime")
    private Long mRuntime;

    @Ignore
    @SerializedName("spoken_languages")
    private List<SpokenLanguage> mSpokenLanguages;

    @ColumnInfo(name = "status")
    @SerializedName("status")
    private String mStatus;

    @ColumnInfo(name = "tagline")
    @SerializedName("tagline")
    private String mTagline;

    @ColumnInfo(name = "title")
    @SerializedName("title")
    private String mTitle;

    @ColumnInfo(name = "video")
    @SerializedName("video")
    private Boolean mVideo;

    @ColumnInfo(name = "vote_average")
    @SerializedName("vote_average")
    private Double mVoteAverage;

    @ColumnInfo(name = "vote_count")
    @SerializedName("vote_count")
    private Long mVoteCount;

    /* public class MovieAllGenres {
        @Relation(parentColumn = "mId", entityColumn = "mId", entity = Genre.class)
        private List<Genre> mGenres;
    } */

    //classe para adicionar os generos na db

    public Boolean getAdult() {
        return mAdult;
    }

    public void setAdult(Boolean adult) {
        mAdult = adult;
    }

    public String getBackdropPath() {
        return mBackdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        mBackdropPath = backdropPath;
    }

    public BelongsToCollection getBelongsToCollection() {
        return mBelongsToCollection;
    }

    public void setBelongsToCollection(BelongsToCollection belongsToCollection) {
        mBelongsToCollection = belongsToCollection;
    }

    public Long getBudget() {
        return mBudget;
    }

    public void setBudget(Long budget) {
        mBudget = budget;
    }

    public List<Genre> getGenres() {
        return mGenres;
    }

    public void setGenres(List<Genre> genres) {
        mGenres = genres;
    }

    public String getHomepage() {
        return mHomepage;
    }

    public void setHomepage(String homepage) {
        mHomepage = homepage;
    }

    public int getId() {
        return mId;
    }

    public void setId(Integer id) {
        mId = id;
    }

    public String getImdbId() {
        return mImdbId;
    }

    public void setImdbId(String imdbId) {
        mImdbId = imdbId;
    }

    public String getOriginalLanguage() {
        return mOriginalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        mOriginalLanguage = originalLanguage;
    }

    public String getOriginalTitle() {
        return mOriginalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        mOriginalTitle = originalTitle;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverview(String overview) {
        mOverview = overview;
    }

    public Double getPopularity() {
        return mPopularity;
    }

    public void setPopularity(Double popularity) {
        mPopularity = popularity;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public void setPosterPath(String posterPath) {
        mPosterPath = posterPath;
    }

    public List<ProductionCompany> getProductionCompanies() {
        return mProductionCompanies;
    }

    public void setProductionCompanies(List<ProductionCompany> productionCompanies) {
        mProductionCompanies = productionCompanies;
    }

    public List<ProductionCountry> getProductionCountries() {
        return mProductionCountries;
    }

    public void setProductionCountries(List<ProductionCountry> productionCountries) {
        mProductionCountries = productionCountries;
    }

    public String getReleaseDate() {
        return mReleaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        mReleaseDate = releaseDate;
    }

    public Long getRevenue() {
        return mRevenue;
    }

    public void setRevenue(Long revenue) {
        mRevenue = revenue;
    }

    public Long getRuntime() {
        return mRuntime;
    }

    public void setRuntime(Long runtime) {
        mRuntime = runtime;
    }

    public List<SpokenLanguage> getSpokenLanguages() {
        return mSpokenLanguages;
    }

    public void setSpokenLanguages(List<SpokenLanguage> spokenLanguages) {
        mSpokenLanguages = spokenLanguages;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTagline() {
        return mTagline;
    }

    public void setTagline(String tagline) {
        mTagline = tagline;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Boolean getVideo() {
        return mVideo;
    }

    public void setVideo(Boolean video) {
        mVideo = video;
    }

    public Double getVoteAverage() {
        return mVoteAverage;
    }

    public void setVoteAverage(Double voteAverage) {
        mVoteAverage = voteAverage;
    }

    public Long getVoteCount() {
        return mVoteCount;
    }

    public void setVoteCount(Long voteCount) {
        mVoteCount = voteCount;
    }


    //genre string

    /*

    public String getGenresNames() {
        return genresNames;
    }

    public void setGenresNames(String genresNames) {
        this.genresNames = genresNames;
    } */


    @Override
    public String toString() {
        return "Movie{" +
                "mAdult=" + mAdult +
                ", mBackdropPath='" + mBackdropPath + '\'' +
                ", mBelongsToCollection=" + mBelongsToCollection +
                ", mBudget=" + mBudget +
                ", mGenres=" + mGenres +
                ", mHomepage='" + mHomepage + '\'' +
                ", mId=" + mId +
                ", mImdbId='" + mImdbId + '\'' +
                ", mOriginalLanguage='" + mOriginalLanguage + '\'' +
                ", mOriginalTitle='" + mOriginalTitle + '\'' +
                ", mOverview='" + mOverview + '\'' +
                ", mPopularity=" + mPopularity +
                ", mPosterPath='" + mPosterPath + '\'' +
                ", mProductionCompanies=" + mProductionCompanies +
                ", mProductionCountries=" + mProductionCountries +
                ", mReleaseDate='" + mReleaseDate + '\'' +
                ", mRevenue=" + mRevenue +
                ", mRuntime=" + mRuntime +
                ", mSpokenLanguages=" + mSpokenLanguages +
                ", mStatus='" + mStatus + '\'' +
                ", mTagline='" + mTagline + '\'' +
                ", mTitle='" + mTitle + '\'' +
                ", mVideo=" + mVideo +
                ", mVoteAverage=" + mVoteAverage +
                ", mVoteCount=" + mVoteCount +
                '}';
    }
}
