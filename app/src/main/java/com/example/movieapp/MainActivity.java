package com.example.movieapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;


import com.example.movieapp.moviedetails.DetailContract;
import com.example.movieapp.moviedetails.DetailFragment;
import com.example.movieapp.moviefavorites.FavoritesContract;
import com.example.movieapp.moviefavorites.FavoritesFragment;
import com.example.movieapp.movielists.MovieListContract;
import com.example.movieapp.movielists.MovieListFragment;
import com.example.movieapp.moviesearch.SearchMovieContract;
import com.example.movieapp.moviesearch.SearchMovieFragment;
import com.example.movieapp.roomdb.IMovieRepository;
import com.example.movieapp.roomdb.MovieRepository;

import java.util.List;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements SearchMovieContract, DetailContract, MovieListContract, FavoritesContract, NavigateListener{


    private final static String TAG = "MainActivity";
    private IMovieRepository repository;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        if (savedInstanceState == null) {
            repository = MovieRepository.getInstance(this);
            repository.getFavoritesList(new CallbackArrayResponse() {
                @Override
                public void onSuccess(List response) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.favorites_fragment, FavoritesFragment.newInstance());
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }

                @Override
                public void onError(String errorMessage) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.favorites_fragment, MovieListFragment.newInstance());
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            });

        }

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.bottom_menu, menu);
        return true;
    }



    @Override
    public void goToDetails(int movieId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.favorites_fragment, DetailFragment.newInstance(movieId));
                //.replace(MovieListFragment.class, DetailFragment.newInstance(movieId));
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void goToFavorites() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.favorites_fragment, FavoritesFragment.newInstance());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }



    //criar metodos do navigatelistener para estes casos todos



    public void onItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.icon_fav:
                goToFavorites();
                item.setChecked(true);
                // TODO: 16/12/2019 ir a base de dados buscar nº de itens, se no retorno for 0, o item pré seleccionado (checked) passa a ser o fragment da lista
                break;

            case R.id.icon_search:
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.favorites_fragment, SearchMovieFragment.newInstance());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                item.setChecked(true);
                break;

            case R.id.icon_lists:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.favorites_fragment, MovieListFragment.newInstance());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                item.setChecked(true);
                break;

                default:
                    onItemClick(item);
        }

    }


}
