package com.example.movieapp.moviefavorites;

import com.example.movieapp.CallbackArrayResponse;
import com.example.movieapp.models.Movie;
import com.example.movieapp.roomdb.IMovieRepository;

import java.util.List;

public class FavoritesPresenter implements FavoritesContract.Presenter {

    private FavoritesContract.View view;
    private IMovieRepository repository;

    @Override
    public void setView(FavoritesContract.View view) {
        this.view = view;
    }

    @Override
    public void setRepository(IMovieRepository repository) {
        this.repository = repository;
    }


    @Override
    public void getFavoriteList() {
        view.showLoading();
        repository.getFavoritesList(new CallbackArrayResponse() {
            @Override
            public void onSuccess(List response) {
                view.showFavoriteList(response);
            }

            @Override
            public void onError(String errorMessage) {
                view.showError(errorMessage);
            }
        });
    }

    @Override
    public void onSwipeRemove(Movie movie) {
        repository.removeMovie(movie);
        repository.getFavoritesList(new CallbackArrayResponse() {
            @Override
            public void onSuccess(List response) {
                view.showFavoriteList(response);
                view.showDialogueRemoved();
            }
            @Override
            public void onError(String errorMessage) {
                view.showError(errorMessage);
            }
        });
    }
}

