package com.example.movieapp.moviefavorites;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.movieapp.ItemClickListener;
import com.example.movieapp.R;
import com.example.movieapp.databinding.MovieItemFavoriteBinding;
import com.example.movieapp.models.Movie;

import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder> {

    private List<Movie> movieLists;
    private Context context;
    private ItemClickListener itemClickListener;

    public FavoriteAdapter(List<Movie> movieLists, Context context, ItemClickListener itemClickListener) {
        this.movieLists = movieLists;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }


    @NonNull
    @Override
    public FavoriteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        MovieItemFavoriteBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.movie_item_favorite, parent, false);
        //View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
        return new FavoriteViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteViewHolder holder, int position) {
        final Movie movie = movieLists.get(position);
        holder.bindData(movie);
    }

    /* public void addAll(List<Movie> movieLists) {
        this.movieLists.addAll(movieLists);
    } */

    @Override
    public int getItemCount() {
        return movieLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public void clear() {
        movieLists.clear();
        notifyDataSetChanged();
    }

    public void addItems() {
        movieLists.addAll(movieLists);
        notifyDataSetChanged();
    }


    public class FavoriteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final MovieItemFavoriteBinding binding;
        private ImageView ivItemPoster;
        private TextView tvItemName;
        private RatingBar rbVote;


        public FavoriteViewHolder(MovieItemFavoriteBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.getRoot().setOnClickListener(this);
            ivItemPoster = binding.ivItemposter;
            //tvItemName = binding.tvItemname;
            //rbVote = binding.rbVote;
        }

    public void bindData(Movie movie) {
        String imageUrl = "https://image.tmdb.org/t/p/w342" + movie.getPosterPath();
        /* RequestManager requestManager = Glide.with(context);
        RequestBuilder requestBuilder = requestManager.load(imageUrl);
        requestBuilder.into(ivItemPoster); */
        Glide.with(context)
                .load(imageUrl)
                .placeholder(R.drawable.baseline_movie_creation_white_bigger)
                .into(ivItemPoster);

        binding.setMovie(movie);
        //de momento esta com o data binding no layout, mas pode estar com os sets no adapter, assim como estao nos fragment
        //tvItemName.setText(movieItem.getTitle());

    }

        @Override
        public void onClick(View view) {
            final int movieId = movieLists.get(getAdapterPosition()).getId();
            itemClickListener.onItemClick(movieId);
        }
    }

}
