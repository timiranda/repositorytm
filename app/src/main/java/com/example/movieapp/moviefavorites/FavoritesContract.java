package com.example.movieapp.moviefavorites;

import com.example.movieapp.models.Movie;
import com.example.movieapp.models.MovieItem;
import com.example.movieapp.movielists.MovieListContract;
import com.example.movieapp.roomdb.IMovieRepository;

import java.util.List;

public interface FavoritesContract {

    interface View{
        void showError(String message);
        void goToDetails(int movieId);
        void showFavoriteList(List<Movie> movieList);
        void showLoading();
        void showDialogueRemoved();

    }

    interface Presenter{
        void setView(View view);
        void setRepository(IMovieRepository repository);
        void getFavoriteList();
        void onSwipeRemove(Movie movie);
    }
}
