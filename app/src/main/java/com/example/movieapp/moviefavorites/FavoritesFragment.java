package com.example.movieapp.moviefavorites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.movieapp.ItemClickListener;
import com.example.movieapp.NavigateListener;
import com.example.movieapp.R;
import com.example.movieapp.databinding.FragmentFavoritesBinding;
import com.example.movieapp.models.Movie;
import com.example.movieapp.roomdb.MovieRepository;

import java.util.List;



public class FavoritesFragment extends Fragment implements FavoritesContract.View, NavigateListener, ItemClickListener {

    private NavigateListener mActivityListener;
    private RecyclerView rvFavorites;
    public FavoriteAdapter favoriteAdapter;
    private FavoritesContract.Presenter mPresenter;
    private LinearLayout liProgress, liError;
    private TextView tvFavorites;
    private ImageView ivError;
    int orientation;
    //List<Movie> movieList;
    int numColumns = 2;


    public FavoritesFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    //aqui tera de levar as strings ou o que for necessario, posteriormente, dentro do new instance
    public static FavoritesFragment newInstance() {
        FavoritesFragment fragment = new FavoritesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        mPresenter = new FavoritesPresenter();
        mPresenter.setView(this);
        mPresenter.setRepository(MovieRepository.getInstance(this.getActivity().getApplicationContext()));
        //LocaleHelper.onCreate(this.getActivity().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentFavoritesBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorites, container, false);

        rvFavorites = binding.rvFavorites;
        liProgress = binding.liProgress;
        tvFavorites = binding.tvFavorites;
        liError = binding.liError;
        ivError = binding.ivError;

        Glide.with(getContext())
                .load(R.drawable.baseline_movie_creation_white_bigger)
                .placeholder(R.drawable.baseline_movie_creation_white_bigger)
                .into(ivError);

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.getFavoriteList();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivityListener = (NavigateListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivityListener = null;
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //open app in any orientation
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //shows horizontal list
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
            rvFavorites.setLayoutManager(linearLayoutManager);
            tvFavorites.setVisibility(View.GONE);

        } else {
            //shows grid list
            final GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), numColumns);
            rvFavorites.setLayoutManager(gridLayoutManager);
            tvFavorites.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void goToFavorites() {
        mActivityListener.goToFavorites();
    }

    @Override
    public void showError(String message) {
        liProgress.setVisibility(View.GONE);
        liError.setVisibility(View.VISIBLE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message)
                .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
    }

    @Override
    public void goToDetails(int movieId) {
        mActivityListener.goToDetails(movieId);

    }

    @Override
    public void showFavoriteList(final List<Movie> movieList) {
        //varying between grid layout and linear layout
        // TODO: 17/12/2019 bug título Favorites às vezes desaparece ao rodar o ecrã 
        orientation = getActivity().getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
            rvFavorites.setLayoutManager(linearLayoutManager);
            tvFavorites.setVisibility(View.GONE);
        } else {
            final GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), numColumns);
            rvFavorites.setLayoutManager(gridLayoutManager);
            tvFavorites.setVisibility(View.VISIBLE);
        }
        liProgress.setVisibility(View.GONE);
        favoriteAdapter = new FavoriteAdapter(movieList, this.getActivity(), this);
        rvFavorites.setAdapter(favoriteAdapter);
        favoriteAdapter.notifyDataSetChanged();

        //on swipe item action
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                //swipe event declared here, goes included in recyclerview widget
                //only thing declared in favorite adapter is getting movie id in event click
                final int pos = viewHolder.getAdapterPosition();
                final Movie movie = movieList.get(pos);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.suredeletefavorites)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mPresenter.onSwipeRemove(movie);
                                //Toast.makeText(getActivity(), "Removed from Favorites", Toast.LENGTH_SHORT).show();
                                favoriteAdapter.notifyItemRemoved(pos);
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                favoriteAdapter.notifyDataSetChanged();
                                dialogInterface.cancel();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                dialog.setCanceledOnTouchOutside(true);
                favoriteAdapter.notifyDataSetChanged();
            }
        };

        //presenter has method to remove item from favorites

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rvFavorites);

    }

    @Override
    public void showLoading() {
        liProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void showDialogueRemoved() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.removed)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
    }

    @Override
    public void onItemClick(int movieId) {
        mActivityListener.goToDetails(movieId);
    }
}
