package com.example.movieapp;

public interface ScrollToRightListener {
    //to get next page
    void onScrollToRight();
}
